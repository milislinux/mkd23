#!/bin/sh

CUR=1.79.2
CATALOG=/etc/xml/catalog

[ -d /etc/xml ] || install -m 0755 -d /etc/xml
[ -f ${CATALOG} ] || xmlcatalog --noout --create ${CATALOG}


xmlcatalog --noout --add "rewriteSystem" \
	"http://docbook.sourceforge.net/release/xsl/${CUR}" \
	"/usr/share/xml/docbook/xsl-stylesheets-${CUR}" \
	${CATALOG}

xmlcatalog --noout --add "rewriteURI" \
	"http://docbook.sourceforge.net/release/xsl/${CUR}" \
	"/usr/share/xml/docbook/xsl-stylesheets-${CUR}" \
	${CATALOG}

xmlcatalog --noout --add "rewriteSystem" \
	"http://docbook.sourceforge.net/release/xsl/current" \
	"/usr/share/xml/docbook/xsl-stylesheets-${CUR}" \
	${CATALOG}

xmlcatalog --noout --add "rewriteURI" \
	"http://docbook.sourceforge.net/release/xsl/current" \
	"/usr/share/xml/docbook/xsl-stylesheets-${CUR}" \
	${CATALOG}
