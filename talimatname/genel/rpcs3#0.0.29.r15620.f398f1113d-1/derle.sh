cd $SRC/rpcs3
  
  COMM_TAG="$(grep 'version{.*}' rpcs3/rpcs3_version.cpp | awk -F[{,] '{printf "%d.%d.%d", $2, $3, $4}')"
  COMM_COUNT="$(git rev-list --count HEAD)"
  COMM_HASH="$(git rev-parse --short HEAD)"
  echo "${COMM_TAG}.r${COMM_COUNT}.${COMM_HASH}"
  
  cp -r /sources/glslang.git $SRC
  cp -r /sources/llvm.git $SRC

  git submodule init 3rdparty/glslang/glslang 3rdparty/llvm/llvm
  git config submodule.3rdparty/glslang.url ../glslang
  git config submodule.3rdparty/llvm/llvm.url ../llvm
  
  SUBMODULES=($(git config --file .gitmodules --get-regexp path | \
    awk '!/ffmpeg/ && !/libpng/ && !/zlib/ && !/curl/ && !/llvm/ && !/glslang/ && !/pugixml/ '))

  # Göreceli bir klasör yolundan https://github.com yoluna dönüştürmemiz gerekiyor
  for ((i=0;i<${#SUBMODULES[@]};i+=2))
  do
    pathid=${SUBMODULES[$i]}
    path=${SUBMODULES[$i+1]}

    git submodule init $path
    urlid=${pathid/%.path/.url}

    # Bu, url'deki son iki yolu alır, yani RPCS3/rpcs3.git
    url=$(git config $urlid | awk -F/ '{print $(NF-1)"/"$(NF-0)}')

    git config $urlid https://github.com/$url
    git -c protocol.file.allow=always submodule update --init --filter=tree:0 $path
  done
  
  git -c protocol.file.allow=always submodule update 3rdparty/glslang/glslang 3rdparty/llvm/llvm

# GLIBCXX_ASSERTIONS'ın istenmeyen iddialara neden olduğu ve rpcs3'ü çökerttiği bilinmektedir

BAD_FLAG="-Wp,-D_GLIBCXX_ASSERTIONS"
  CXXFLAGS="${CXXFLAGS//$BAD_FLAG/}"

  CC=clang CXX=clang++ cmake -S $SRC/rpcs3 -B build \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_SKIP_RPATH=ON \
    -DUSE_NATIVE_INSTRUCTIONS=OFF \
    -DUSE_SYSTEM_FFMPEG=ON \
    -DUSE_SYSTEM_LIBPNG=ON \
    -DUSE_SYSTEM_ZLIB=ON \
    -DUSE_SYSTEM_CURL=ON \
    -DUSE_SYSTEM_FLATBUFFERS=OFF \
    -DUSE_SYSTEM_PUGIXML=ON \
    -DBUILD_LLVM=ON \
    -DUSE_SYSTEM_WOLFSSL=OFF
  
  make -C build
