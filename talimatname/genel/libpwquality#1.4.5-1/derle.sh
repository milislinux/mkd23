export LIBRARY_PATH="$LIBRARY_PATH:$PKG/usr/lib"
export CFLAGS="$CFLAGS -I$PKG/usr/include" 
export CFLAGS="$CFLAGS -I$PKG/usr/include/xfce4" 
export PKG_CONFIG_PATH=$(pkg-config --variable pc_path pkg-config):$PKG/usr/lib/pkgconfig/
export LD_LIBRARY_PATH=/usr/lib:$PKG/usr/lib
export XDG_DATA_DIRS=/usr/share:$PKG/usr/share

cd $SRC

cd cracklib-*
./configure --prefix=/usr --sbindir=/usr/bin --without-python
make $MAKEJOBS && make install DESTDIR=$PKG
mkdir -p "$PKG"/usr/share/dict
  ln -sf /usr/share/cracklib/cracklib-small "$PKG"/usr/share/dict/cracklib-small
  sh ./util/cracklib-format dicts/cracklib-small \
    | sh ./util/cracklib-packer "$PKG"/usr/share/cracklib/pw_dict

cd -
cd libpwquality-*
autoreconf -fi
./configure --disable-static --enable-pam --with-securedir=/usr/lib/security
make $MAKEJOBS
