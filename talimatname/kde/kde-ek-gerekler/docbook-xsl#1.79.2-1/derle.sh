cd $isim-nons-$surum

	patch -Np2 -i $SRC/non-recursive-string-subst.patch
	install -v -m 0755 -d $PKG/usr/share/xml/docbook/xsl-stylesheets-$surum

	cp -v -R \
		VERSION common eclipse epub epub3 fo \
		highlighting html htmlhelp images javahelp lib manpages \
		params profiling roundtrip slides template tests tools website \
		xhtml xhtml-1_1 xhtml5 \
		$PKG/usr/share/xml/docbook/xsl-stylesheets-$surum

	ln -s VERSION $PKG/usr/share/xml/docbook/xsl-stylesheets-${surum}/VERSION.xsl

	ln -sf xsl-stylesheets-$surum $PKG/usr/share/xml/docbook/xsl-stylesheets

	find $PKG \( -name "NEWS*" -o -name "README*" -o -name "ChangeLog*" \) -delete
