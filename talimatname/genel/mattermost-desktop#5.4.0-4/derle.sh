_electron=electron24
_archive="${isim#*-}-$surum"
_npmargs="--cache '$SRC/npm-cache' --no-audit --no-fund"

cd "$SRC/$_archive"
	sed -i -e "s/git rev-parse --short HEAD/echo $surum/" webpack.config.base.js
	sed -e "s/@ELECTRON@/$_electron/" "../$isim.sh" > "$isim.sh"
	local _electronVersion="$(< "/usr/lib/$_electron/version")"
	jq '	.linux["target"] = [ "dir" ]' electron-builder.json |
		sponge electron-builder.json
	jq '	.devDependencies["electron"] = $electronVersion |
		del(.devDependencies["electron-rebuild"]) |
		.config.target = $electronVersion |
		.config.runtime = $electronRuntime' \
			--arg electronRuntime "$_electron" \
			--arg electronVersion "$_electronVersion" \
			package.json |
		sponge package.json
	sed -i -e '/package:/s/tar.gz deb rpm/dir/' package.json
	npm $_npmargs install

	export NODE_ENV=production
	npm $_npmargs --offline run build
	npm $_npmargs --offline run package:linux-all-x64
