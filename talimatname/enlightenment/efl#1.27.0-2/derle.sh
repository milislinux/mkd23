export CFLAGS="$CFLAGS -fvisibility=hidden"

if [ -d build ]; then
rm -rf build
fi
mkdir -p build

meson --prefix=/usr \
-Dfb=true \
-Ddrm=true \
-Dwl=true \
-Dglib=true \
-Dnetwork-backend=connman \
-Dbindings= \
-Dbuild-examples=false \
-Dbuild-tests=false \
-Decore-imf-loaders-disabler=ibus,scim,xim \
-Dpulseaudio=true \
-Ddocs=false \
. build

ninja -C build
