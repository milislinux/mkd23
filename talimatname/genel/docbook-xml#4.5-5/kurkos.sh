#!/bin/sh

CATALOG=/etc/xml/catalog
isim=docbook-xml

if [ ! -f "$CATALOG" ]; then
	mkdir -p /etc/xml
	xmlcatalog --noout --create "$CATALOG"
fi

xmlcatalog --noout --add "delegatePublic" \
	"-//OASIS//ENTITIES DocBook XML" \
	"file:///etc/xml/$isim" $CATALOG

xmlcatalog --noout --add "delegatePublic" \
	"-//OASIS//DTD DocBook XML" \
	"file:///etc/xml/$isim" $CATALOG

xmlcatalog --noout --add "delegateSystem" \
	"http://www.oasis-open.org/docbook/" \
	"file:///etc/xml/$isim" $CATALOG

xmlcatalog --noout --add "delegateURI" \
	"http://www.oasis-open.org/docbook/" \
	"file:///etc/xml/$isim" $CATALOG
